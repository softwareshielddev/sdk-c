/**
 * 
 *
 Created on: Mar. 03, 2013 
 Updated on: Feb. 01, 2014
 *      
 *
\mainpage GameShield v5 SDK Programing Guide
\tableofcontents
 
\section intro_sec Introduction
 
 GameShield v5 is completely designed and coded from scratch to achieve the following goals:
 
  - Cross-Platform : Windows, MacOS and Linux
  - Flexible License Models : Micro-transaction to license multiple entities.
  - Extensibility and Simplicity
 
 GameShield V5 software is a cross-platform *DRM* (*Digital Rights Management*) solution for applications. 
 Although it is developed with Game protection in mind, it is a generic DRM solution for all kinds of software. 
 In this document, both game and application refer to the same thing --- the target software to be protected.

 \subsection ssecFeatures Features
- Operating System Supported: 
	+ Windows: WinXP and above; 
		Ironwrapper/Win supports only 32bit, SDK supports both 32bit and 64bit.
	+ Mac OS: 10.5 (Leopard) and above, Intel CPU 32bit/64bit;
		Apple App Sandbox compatible.
	+ Other Unix-like OS (Linux, FreeBSD, SunOS, etc.):	
		No Ironwrapper support, SDK supports 32bit/64bit.
	
- Native Code Performance: All modules (except GS5 IDE in C\#) are developed in native C++ / Pascal for maximum runtime performance
- Non-intrusive Software Protection:  No need to touch game source code, just specify the license model logic and wrap it!
- Extensibility: with GS5/SDK developers can develop any license model logic of their own and integrate with the software easily.
	GS5/SDK supports most commonly used languages (C/C++/C\#/Object-C/Pascal/Java), the api interface is quite similar for different languages.
- High Security: Activates game with short string code (Serial Number, Activation Code) or DSA (Digital Signature Algorithm) based key file.  
- Dual-Activation Mode: Games can be activated both on-line and off-line.
- GS5 can protect software developed in C/C++/Pascal/java/Flash;
- GS5 can protect software with multiple executables/dlls (Exe-Hopping);
- GS5 can protect software all kinds of game data files thanks to its wrapping technology.

\subsection secWorkflow Workflow
GameShield V5 works as following:
\image html GSOverview.png
\image latex GSOverview.eps "Overview" width=8cm

- Copy original *game source* (we mean the game binaries, not game source code :) to a local disk folder;
- Create a GS5 project in GameShield IDE, defining the entities, license models and most wrapping features in GUI;
- After building the project in IDE, the IDE generates the following output in the project folder:

\image html ProjectLayout.png
\image latex ProjectLayout.eps "Project Folder Layout" width=8cm

- *XXX.lpj* file: The project file. It is a plain text file in XML format;
- *Tmp/* directory:

  All temporary files are stored here, including wrapping script files (iw.xml, gsClean.xml, gsKeyGen.xml) and wrapping output log file (iw.log), also on Windows there is a batch file (iw.bat) that you can invoke it to re-wrap the game manually on command line without a full rebuild from IDE.
	
- *Release/* directory:
  	+ License File (XXX.lic) and its corresponding KeyGen file (XXX.lic.kg)
  		The *license file* is a piece of binary data in an encrypted format containing all needed information at client side, it is wrapped later in the final protected game binaries in the wrapping process. 
  		
  		The *KeyGen file* stores all needed information for key generation, it has two usages:  
		1. The IDE uploads the keygen data to CheckPoint server database; at runtime, it is used to generate activation code on game's demand.
		2. The IDE wraps the keygen data into KeyGenerator, so that the gsKeyGen utility can generate activation code manually for this game.
  		
  	+ *Deploy/* directory: Keeps the protected game 
  	  Only the content under Deploy folder can be delivered to public customers, all other stuffs under Release folder should be kept for internal usage only.
  	  
  	+ *KeyClean/* directory:  Clean up local license storage
  		This utility (gsClean) can be used to reset or clean up local license storage which is quite useful when QA a wrapped game. After gsClean runs, all of the existent license data for this game is erased, so that when the game runs, it runs as if it is a new installation on this machine. 
  		
  		It is not recommended to ship this utility to public users; the official way of license resetting is applying a clean up action (*ACT_CLEAN*), please refer to LMApp and Custom SDK Developing for details.
  		
  	+ *KeyGenerator/* directory: Manual Key/Activation Generator for this game.
		It is a QT-based desktop utility to generate activation code for a valid request code for off-line activation. For on-line activation, it is not used at all, the activation code is transferred via internet web services.
  
- *Sys/* directory:
  Internal folder to keep project build history and backups (not implemented yet). 
- *Src/* directory:
  Keeps a full copy of the original game source binaries. 
 


\subsection secSoftwareStack Software Stack
The GameShield product consists of multiple software components partitioned in stacked layers.

\image html SoftwareStack.png
\image latex SoftwareStack.eps "GameShield Software Module Stack" width=8cm

#### High Layer: Cross-platform Modules

- gsCore: The Core library implements the SDK APIs.
	Developed in C++, **gsCore** implements all licensing logics and exports more than 100 apis to support other higher level modules. All operating system specific features are encapsulated in gsCore so that all software modules based on gsCore can be cross-platform.

- gsRender: HTML User Interface Render
	gsRender implements a programming interface for HTML page rendering; it also exports GS5 core objects (entity, action, license, variable, etc.) as Javascript objects so that you can access GS5 features in pure cross-platform Javascript. 
	
	Currently gsRender is Qt/WebKit based and is completely self-contained and cross-platform, maybe later GS5 will provide OS-dependent gsRender version to avoid Qt runtime delivery with wrapped game.     
	
- Project IDE
	Basically the IDE (*Shield.IDE*) works as a XML editor and a high-level UI presentation layer, it creates / edits project file (*.lpj, a XML text file), specifies entities and licenses. The IDE comes with a set of built-in License HTML templates. The UI can be previewed (via gsRender) and the template settings can be customized.The IDE calls **IDEConnect** library	to compile project. You can also invoke a command line version (**Shield.CMD**) to build the project in a batch or script file.
			
- LMApp (License Model Application)
	+ **gsLMRun**: License Model Application (LMApp) execution engine
	 	It parses the license model application's configuration file (lmapp.config) and drives the UI pages for different system events.
	 	If your UI is not LMApp framework based, gsLMRun is not needed. You can install event listeners and implement your own event handlers and display UIs in your own way.
	 		
	+ **LMApp Framework**
	 	It is a built-in XML/HTML/Javascript based framework that can be customized and previewed in IDE.
	
- SDK (GS5 Software Development Kit)
	+ **GS5_Intf**: Non-OOP interface to gsCore's flat-C apis
	+ **GS5**: OOP framework based on GS5_Intf
	+ **GS5_Ext**: Custom License Model Development
	
- Desktop Utilities
	+ **gsManualActivator**: Manual Key Generator
			
	A GUI utility to generate license code. When the user needs to activate or extend the
	accessibility of product features, a special magic string (called **License Request Code** is generated automatically by built-in license UI), it is then transfered to software vendor
	or sales dealers who produce the corresponding **License Code** for the customer with ManualActivator. The user then input the
	returned code to apply the change to local license status.
					
	If the project is pre-defined to use CheckPoint server, the above round-trip happens in 						
	background with less user interaction. The user needs to input
	a **SN :Serial Number** then everything happens automatically.
			
	+ **gsClean**: License Cleaner
		
	
- gsKeyGen: Server Side Key Generator
		It is called by server application to generate activation code for wrapped games on-line.


####Ironwrapper Layer
	- Ironwrapper for Windows
	  + gsLoader.dll: bridges C++ interfaces and Low-level interfaces from Ironwrapper kernel.
	  + iw4win.exe: The low-level magic box consists of multiple coders, linker and loader. It accepts iw.xml and build the wrapped output binaries.

	- Ironwrapper for Mac
	  + item AppHelper: Application Helper
		It encapsulates MacOS-specific apis.   
	  + yloader: Wrapped game loader
	  + iw4mac: The game wrapper





\subsection sec_diff Differences between GameShield v5 and old versions
    Because the project goals and implementation is quite different, there are many major feature differences listed as following:

|     | GameShield v5     |  SS/GS 3/4.x
----- | ----------------- | ----------------------------------------------------
Working Platform  | Windows, Mac and BSD/Linux variants | Windows Only
Developing Toolsets | IDE (C#), Core (c++), Ironwrapper(c++, pascal) | Borland C++ Builder/Delphi
SDK deployment  | Core APIs are in pure C, SDK ships various thin wrappers for source integration, no need for special setup or registration | SSCProt.dll/SSCProt.exe is COM component, must be registered before use and might cause version conflicts and compatibility issues (Thread-Model, user privilege, etc.)
Micro-Transaction | Unlimited entities can be licensed or protected in a single project | Only single entity (aka the Application itself) is protected
Project File Format | Portable XML | Private binary format, very hard to extend and maintain backward compatibility
License File Format | Private binary format, cross-platform portable, easy to extend and maintain binary compatibility | Same as old Projet File, very hard to extend and maintain
License File Deployment | License File can be deployed \a ReadOnly, or just embedded in AXS. | License File must be deployed in ReadWrite mode and its access has to be synchronized carefully for multiple processes/threads.
KeyGen File | Private binary format, its format is very stable and it is uploaded to CP server or embedded in ManualActivator, hence the possibility of server side upgrade or licenseCode format mismatch is minimized. | N/A, the volatile Project File is used instead, so the server side always need to upgrade for each GS/SS version
Virgin Data | obsolete in IDE/UI, it is implemented at the core level in a platform-specific safe manner | Incorrect Virgin Data settings cause problem in Vista+ (permission issue)
Alias Files | obsolete in IDE/UI, it is implemented at the core level in a platform-specific safe manner | Incorrect Alias files settings causes problem in Vista+ (permission issue)
Authorization Definition | Removed. Now the license \a actions play the same role at the core level. | The most hard to use UI part in IDE.
License Code Capacity | GS5 embeds unlimited license actions in a single license code. The ManualActivator is intuitive to use thanks to the License/action class reflection feature. | The LicenseCode (aka ActivationCode) can only embed limited /pre-defined license operations (AuthDef), ManualActivator is hard to use.
License Code Algorithm | Classical variable-length SN and modern DSA, the DSA-based licenseCode is extremely hard to crack in theory | Fixed-length SN, easy to write a keygen by reverse-engineering.
What to do when license missing or corrupted | Can always roll-back to the original license demo status. It's useful to copy games to friends to play in demo mode.| Fails to launch or try to recover license manually or automatically. If all of the alias files and license file are corrupted, manual recovery is the only choice.
License Status Checking | Checking the latest status first, if corrupted, fall-back to the previous copy. It is very stable and error-tolerant even in unexpected machine reboot / crash | All alias files must be consistent with the master license file; The algorithm is pretty error-prone and likely to report false-positive if machine is rebooted unexpectedly or AV products interfere with the disk IO.
FingerPrint | Error-Tolerant by design. License is still valid if only a small change applied to hardwares. | Zero Error-Tolerant. A single change in hardware will invalid a license. Sometimes the fingerprint changes come from our internal algorithms or Windows OS behavior difference.

\section Basic Concepts
Before continue we must clarify the basic terminology and new concepts introduced in GS5 first.

\subsection sec_ProductName Product Name
\anchor ProductName

ProductName is your application name or game title, it is a user-friendly string that can be displayed as UI form caption.
\see gs::gsGetProductName()
\see gs::TGSCore::getProductName()

\subsection sec_ProductId Product ID
\anchor ProductId

In GS5, a product is associated with a world-wide unique string identifier, usually
it is a UUID string (9d062304-6249-4180-9b84-46c6ea5ec916) and can be used as a
primary database key in local or server side license store.

\subsection sec_buildId Build Id
\anchor BuildId

BuildId is an auto-increased integer version tag for each game *build* in IDE. When game runs, the \a buildId is used by GS5 core to upgrade
old license status from local storage, at server side, the buildId helps the server locate the correct version of keygen data.

  Since GS5/IDE 5.2.1 (Dec. 16, 2013) the IDE retrieves the latest BuildID from CheckPoint server for every build; 
    It ensures the buildId is unique and incremental for each project.




 \subsection ent_sec Entity
It is a new concept in GS5. An entity is a conceptual object that can be physically mapped to almost anything that need to be protected or eligible for licensing and sale.
For example, an entity can be a game level, a costly game weapon, a package of game maps or add-ons for a online game. the entity might also be a whole book, a single chapter without the corresponding license you cannot read it;
You can define any file as an entity so its accessibility is license-controlled. You can even assign parts of a video file to different entities so you enjoy a movie in pay-as-you-go manner.
 

In GS5, there are multiple entities in a single project. Currently, the entities is non-overlapping which means the license status change of one entity will not affect other entities. Of course it does not mean you cannot issue licenses in bundles. For instance,
in the scenario of eBook protection, you can virtually define a special *big* entity that is mapped to
the whole book, and defines 10 *small* entities for individual 10 chapters, and when the user has paid for the *big* entity, you automatically unlock all chapters in the ad hoc license model.
    		
By design, we intentionally split the low-level license logic unit as simple as possible and try to move the complex business logic to the upper-level (License Model layer),
it makes the whole system more extensible and efficient.
 
\anchor entityId
 Each entity is assigned an application-wide unique string identifier called Entity ID,
it is used as the only key to retrieve any entity-related information from the internal
system database. Entity Name and Entity Description are simply friendly and optional
string properties to display in IDE.
The (productId, entityId) pair works as database key to locate an entity.

\anchor EntityAttr
*Entity Attribute*:  
 
Entity is a license-driven finite state machine having the following states:

- ENTITY_ATTRIBUTE_ACCESSIBLE (1): Entity is currently accessible; all of the license(s) attached to the entity approve that the entity can be accessed, the license might have been unlocked (fully purchased)
or is still in the trial period. 
\see gs::TGSEntity::isAccessible()

- ENTITY_ATTRIBUTE_UNLOCKED (2): Entity's license is fully activated, no expire /trial limits at all.
\see gs::TGSEntity::isUnlocked()

- ENTITY_ATTRIBUTE_ACCESSING (4) Entity is active (being accessed via gsBeginAccessEntity())
\see gs::TGSEntity::isAccessing()

- ENTITY_ATTRIBUTE_LOCKED (8) Entity is locked down. An entity can be locked down by applying action ACT_LOCK or after license's trial period expires. Entity can leave locked state by applying an ACT_UNLOCK action, or
a license-specific action (i.e. increase the trial period, trial times, etc.) 

\see gs::TGSEntity::isLocked()
\see gs::gsGetEntityAttributes
\see gs::TGSEntity::attribute()

\anchor LicensePolicy
* License Policy* 
		
Defines the combination policy of the multiple licenses attached to a single entity

- POLICY_ANY: 
Entity can be accessed if *any* of the associated licenses is valid (Default)
- POLICY_ALL: 	
Entity can be accessed only when *All* associated licenses are valid 

 *   	\subsection lic_sec License
 *			A license is *not* a piece of plain / dead data file that is parsed and verified for access control. In GS5, you can think of a license as a pretty dynamic and self-contained logic black-box, the inputs might be local hardware finger prints,
 *			or current date time or any interested parameters, the output is a single boolean signal of VALID or NOT-VALID.
 *
 *			The term <i><b>License Model</b></i> defines a specific business logic and acts as a class template, the term <i>License</i> is an concrete instance from its corresponding <i>License Model</i>
 *
 *			A license can be attached to only one entity; if the TGSLicense::isValid() returns true (VALID) then you may think \a the license think it OK to access the associated entity for now.
 *			On the other hand, one entity can be associated with \a *multiple* licenses at runtime. Whether the entity can be finally accessed depends on an entity-specific flag called <i>License Policy</i> (refer to gsGetEntityLicensePolicy()). If the current license policy is POLICY_ANY, then
 *			the entity can be legally accessed if any of the associated licenses is in VALID status, otherwise in POLICY_ALL mode, all licenses must be VALID before the controlled entity can be accessed.
 *
 *
 *		    A license has multiple properties (\a LicenseName, \a LicenseId and some other business logic related parameters). In GS5 all built-in license types (aka License Model) supports
 *		    introspection, that is, all of the license properties (and usable actions) can be enumerated and discovered programtically.
 *
 *		    The license defined in IDE is called *STATIC* license since its status is always persistent in local license database, there is another kind of license called *DYNAMIC* license which exists in memory temporarily and attach to the target entity
 *		    on the fly, usually this kind of license is created at runtime or transfered from the remote server to change the application's license status for a short term (per game session maybe).
 *
 *		    When a license is valid it might optionally \a push or \a publish some kinds of otherwise private information to the running environment. For example, a license might inject several predefined decryption keys to the active key-store so that those
 *		    associated data files can be decrypted correctly. Those keys might also be part of LicenseCode payload streamed from remote server and they are destroyed once game terminating.
 *
 *
		\anchor LicenseStatus
		\subsubsection LicenseStatus
		  - STATUS_INVALID  License status is not currently returned due to unexpected error, i.e. the result returned is a useless garbage value.	
		  - STATUS_LOCKED   the license is disabled permanently. 
		  - STATUS_UNLOCKED the license is unlocked (fully activated, or purchased) permanently. 
		  - STATUS_ACTIVE the license is not locked or unlocked by actions, isValid() works by its own logic;


 *		\subsection act_sec Action
 *			A license action is an atomic operator that can be applied to a license to change its current license status.
 *			For example, you can use actions to lock /unlock any license, reset or extend expiry settings of demo licenses.
 *			The license status can only be \a officially modified by itself (according to its own business logic) or by license actions.
 *			Multiple actions can be coded in a single requestCode or licenseCode for a batch operation.
 *
 *
 *			\anchor ActionInfo
			\subsubsection ssecLicAct License-Specific Action

Action can be license specific, which means, an action can only be applied to some kinds of license models. 
When developing a license model, the developer can develop ad-hoc actions for it so that the license (status, parameters) can be manipulated by 
these actions.

Given a license, GS5 SDK provides apis to enumerate *all* available actions (both universal and license-specific) that can be applied to
this license.

			\code
				int N = gsGetActionInfoCount(hLicense);
				for(int i = 0; i < N; i++){
					action_id_t actId;
					const char* actName = gsGetActionInfoByIndex(hLicense, i, &actId);
					cout << "action name " << actName << "action id" << actId <<std::endl;
				}
			\endcode

		\anchor WhatToDoActionString
		\subsubsection secWhatToDo What to Do Action String


 *
 *   	\subsection reqCode_sec Request Code
 *  \anchor requestCode
 *   		Request Code is a container of \a *desired* license actions to be taken to local licenses. It can be composed from LicenseAssistant or Wrapped game's \a BuyNow UI,
 *         it effectively tells the game vendor or service provider <i>What I want to do</i> to the current licenses.
 *
 *         Request code also contains various useful hidden information needed by the DRM facilities.

           Depending on its content, the length of requestCode string is not fixed, it is also grouped in 5 characters as following:

		   *CLC6O-NBMHP-XHOTR-6DMGF*	  
		   
		   *CE4P9-TRE7Z-IA6ES-O6XXQ*


		   The request code is generated from a TGSRequest object, you can create a TGSRequest object and adds one or more actions to it, and then
		   calls the TGSRequest::code() api to retrieve the request code.

		   The OOP-SDK has implemented helper apis to get commonly used request code:

		   \code

		   TGSCore * core = TGSCore::getInstance();
		   const char * requestCode;

		   //Gets the request code to clean up local license storage
		   requestCode = core->getCleanRequestCode();

		   //Gets the request code to unlock the whole application
		   requestCode = core->getUnlockRequestCode();

		   //Gets the request code to unlock the first game level only
		   std::auto_ptr<TGSEntity> entityLevelOne (core->getEntityById("Game Level 1"));
		   requestCode = entityLevelOne->getUnlockRequestCode();

		   //If the first game level entity is attached several licenses, and we only want to unlock the first license: 
		   //Gets the request code to unlock only a single license
		   std::auto_ptr<TGSLicense> myLicense(entityLevelOne->getLicenseByIndex(0));
		   requestCode = myLicense->getUnlockRequestCode();

		   \endcode

 *
 *   	\subsection licCode_sec License Code (aka.  "Authorization Code",  "Activation Code")
 *			Like requestCode, the liceneCode is also a container of multiple \a *approved* license actions. It is issued from the ManualActivator or
 *			remote server after the requestCode is parsed and verified (and modified if necessary). Basically it tells the client side <i>What you can do</i> to the current licenses.
 *
 *			licenseCode also optionally contains some license-specific pay-loads that can be extracted to the local system when the contained license codes are applied.
 *

#### DSA-based KeyGen Algorithm
  DSA (Digital Signature Algoruthm) is a built-in KeyGen algorithm that can be enabled in GS5 IDE:

  \image html ide_dsa.png
  \image latex ide_dsa.jpg "DSA ALgorithm" width=10cm


  When DSA-KeyGen is enabled, a _public_ key is stored as part of local license status, and the corresponding _private_ key is stored in keygen data that is uploaded
  to the server side, each activation code generated from server side will be digital-signed so that it is hard for the crackers to write a fake keygen without 
  the private key.

#### License Code Length and Format

Depending on the KeyGen algorithm selected in the project settings in IDE, the licesne code is either a short string or a text license file.
- non-DSA KeyGen algorithm:
	Licene code is a short string that is grouped in 4 characters as following:
	*BY7T-35BI-9EBP-FNUM-EPUB*

- DSA KeyGen algorithm:
	License code is a long string that is better to be transferred as a text file:

	Tn2ga-9osNG-W6lI3-8@1FW-KeUi0-IxCEB-oebdK-QEOAX-K%4XT-6szm2-cHsIK-YqZUh-IBgU@-oZJ9q-gCK13-zScZr-m9WKz-Upk2



#### Apply License Code

Applying a license code to modify the local license status is pretty simple:

\code
    TGSCore::getInstance()->applyLicenseCode( licenseCode );
\endcode

At runtime, after a license code is applied, all affected entities will trigger EVENT_ENTITY_ACTION_APPLIED, so that you can update the license
status on UI for an entity.




		\subsection secVar Variables and Parameters
		 In GS5, a variable is a simple object which has:
		 - name Variable string name;
		 - typeId: Variable data type id;
		 - value: Variable value;
		 - attribute: The meta data defining if the variable can be read, write and how it is peristed, etc.
		 
		 There are three categories of variables in GS5:
		 

*		 - User Defined Variables
*		   \anchor UsrVar
*		   You can define variables in IDE, or define them via gs::gsAddVariable();
*
*		 - License / Action Parameters
*
*		 - Application Session Variables
*		   \anchor AppVar
*		  xxx   
*		
*
*		\anchor varType
*		###Variable Data Type
*       
*       Data Type | Type Id  | Type String
*      ---------- | -------- | -----------
*       int32     | VAR_TYPE_INT32 (7) |"int32"
*       int64   | VAR_TYPE_INT64 (8) | "int64"
*       float   | VAR_TYPE_FLOAT (9) | "float" 
*       double  | VAR_TYPE_DOUBLE(10) | "double"
*		bool    | VAR_TYPE_BOOL (11) | "bool"
*       string  | VAR_TYPE_STRING (20) | "string"
*      utc time | VAR_TYPE_TIME (30) | "utc_time"
*
*		\anchor varAttr
*		###Variable Attribute
*		Attribute    |   Mask  | String | Description 
*	   ------------- | ------- | ------ | -------------
*	    Read         |  0x01   |  r     |Variable is readable
*		Write        |  0x02   |  w     |Variable is writable
*		Persistent   |  0x04   |  p     |Variable is persisted (not a temporary one)
*		Secure       |  0x08   |  s     |Variable is secured in memory
*		Remote       |  0x10   |  R     |Variable is persisted at server side
*		Hidden       |  0x20   |  h     |Variable cannot be enumerted via apis
*		System       |  0x40   |  S     |Variable is reserved for internal system usage
*
*


\section secEvents Events

GS5 uses event-listener design pattern, there are 3 different public event sources:

\subsection secAppEvent Application Events

- EVENT_APP_BEGIN (1): Application just gets started
	
	At this stage all dependent libraries are loaded and the application is about to run.
	
	The Licensing system is already initialized and the license store is connected, so any licensing related information
	is available.

- EVENT_APP_END (2) : Application is going to terminate
	
	It's the last event sent out to all listeners, the licensing system has shut down and the application is about to exit. 
	No licensing information can be retrieved at this stage.

- EVENT_APP_CLOCK_ROLLBACK (3) : Alarm: Application detects the clock is rolled back
	
	When the system clock is rolled back, the application will be terminated after this event is handled.
	
	GS5 has several internal event sources to check the clock rollback behavior and all of them are merged into this public event, so
	 the developer should only listen to this one to catch all internal events.

- EVENT_APP_INTEGRITY_CORRUPT (4) : Alarm: Application's integrity is corrupted. Usually it means that some read-only files which are monitoed
   by GS5 *is* changed from the original file state when the game is wrapped.
	When the system integrity is corrupted, the application will be terminated after this event is handled.

- EVENT_APP_RUN (5) : Application is going to run. It's the last event sent before the OEP (original entry point) is executed.
	
	At this point the Licensing logic (LM's UIs have been popped up and have completed the interaction with the end users) should have
	been processed and it is a good time to cleanup any unused stuffs while application is running.


\subsection secLicenseEvent License Events
- EVENT_LICENSE_NEWINSTALL (101) : Original license is uploaded to license store for the first time.
- EVENT_LICENSE_READY (102) : The application's license store is connected /initialized successfully (gsCore::gsInit() == 0)
- EVENT_LICENSE_FAIL (103)  : The application's license store cannot be connected /initialized! (gsCore::gsInit() != 0)


\subsection secEntityEvent Entity Events

Entity events are posted while internal state transition happens either by SDK api calling or license state changes.

\image html entity_event.png
\image latex entity_event.eps "Entity Events" width=2cm

- EVENT_ENTITY_TRY_ACCESS (201): The entity is to be accessed.

  The listeners might be able to modify the license store here,
  The internal licenses status is not touched yet. (inactive if not accessed before)
- EVENT_ENTITY_ACCESS_STARTED (202): The entity is being accessed.
  The listeners can enable any protected resources here. (inject decrypting keys, etc.)
  The internal licenses status have changed to active mode.
- EVENT_ENTITY_ACCESS_ENDING (203) : The entity is leaving now.
  The listeners can revoke any protected resources here. (remove injected decrypting keys, etc.)
   Licenses are still in active mode.
- EVENT_ENTITY_ACCESS_ENDED (204) : The entity is deactivated now.
The listeners can revoke any protected resources here. (remove injected decrypting keys, etc.)
  License is switched to inactive mode.
- EVENT_ENTITY_ACCESS_INVALID (205) : Entity access is expired or becomes invalid for some reason.
  In current implementation, once the event is posted, the application will be terminated so the listeners
  should take this chance to display a license UI before the application quits silently.
- EVENT_ENTITY_ACCESS_HEARTBEAT (206):  event indicating entity is still alive.

- EVENT_ENTITY_ACTION_APPLIED (208): The status of attached licenses have been modified by applying license action.
It is called after the change has been made, so you can have a chance to read latest license status to update in-memory variables.
gsLMRun uses this event so once the end user has activated the product no more nag screen should be popped up.




\section secBuildInAct Built-in Actions

\subsection secUnLock act_unlock (1)
Sets a license's lock state to *STATAUS_UNLOCKED* so the target license's internal logic is shortcuted and always returns true.

If the protected entity has only one *unlocked* license, or its license policy is POLICY_ANY, then the entity can be accessed any time anywhere(
protection is completely unlocked). We can use this action to implement the *fully-purchase* feature.

- Action ID: ACT_UNLOCK = 1
- Params: None
- One Shot: No

\subsection secLock act_lock (2)
Sets a license's lock state to *STATAUS_LOCKED* so the target license's internal logic is shortcuted and always returns false.

If the protected entity has only one *locked* license, or its license policy is *POLICY_ALL*, then the entity cannot be accessed 
(completely locked).

- Action ID: ACT_LOCK = 2
- Params: None
- One Shot: No

\subsection secEnableCP act_EnableCopyProtection (6)
Turns on the targeting license's *copy protection* feature. 
When a license is *copy protected*, the application can run only on a specific machine.

- Action ID: ACT_ENABLE_COPYPROTECTION = 6
- Params: None
- One Shot: No

\subsection secDisableCP act_disableCopyProtection (7)
Turns off the targeting license's *copy protection* feature. 
When a license is not *copy protected*, the application can run on any machine.

- Action ID: ACT_DISABLE_COPYPROTECTION = 7
- Params: None
- One Shot: Yes

\subsection secResetExpr act_resetAllExpiration (10)
Resets all expiration settings to its initial value. 

- Action ID: ACT_RESET_ALLEXPIRATION = 10
- Params: None
- One Shot: Yes

For example, if this action is applied to a "Expire By Access Times" license, the interal parameter of "usedAccessTime" is set to zero;
when applied to "Expire By Period" license, the first access date time is set to current time, and the ending date is adjusted accordingly.


\subsection secClean act_clean (11)
Clean up the local license storage as if the application was not installed before.
- Action ID: ACT_CLEAN = 11
- Params: 
	+ endDate : UTC Datetime, specify the hard-coded expiration date of the action.

	By default, the parameter does not hold a valid date time, so there is no expiration date for this action; However, because this action
	resets the local license status to its initial state, most likely you do not want the game player do this multiple times for a trial license model,
	so you should set the __endDate__ to an appropriate time to prohibit resue of the the same clean code.
		
- One Shot: No
  
   The reason why this action cannot be an one-shot action is that after the action is applied, the current license status is purged including the information of
   used license codes, so the use of clean action has to be restricted by a hard-coded shelf-time.

__Warning__:

  After this action is applied, the application should be terminated or restarted immediately. 

\subsection secFPFix act_fpFix (19)
Fix the "FingerPrint Mismatch" issue.

- Action ID: ACT_FP_FIX = 19
- Params: None
- One Shot: Yes

By design, when a game is node-locked and launches, the current hardware's fingerprint is compared to the original fingerprint status snapshoted when the game first run. if, for some reason (hardware upgrade,
copied from another machine, etc.) the fingerprint does not match then the license fails to load and the game cannot run. 

To help valid customer overcome this issue, this action can be applied to update the current fingerprints to make sure the hardware matches the license.

This is a _one-shot_ action so the license code hosting act_fpFix can only applied once for a single machine.

\section secBuiltInLM Built-In License Models

GS5 has implemented some commonly used licensing logic units, you can use them directly or combine them as one composite license logic.

\subsection secLMExpireAccessTime Expire by Access Times

- LicenseID: gs.lm.expire.accessTime.1
- Description: The protected entity cannot be accessed after it has been accessed more than a preset times
- Function:
\f[
f(usedTimes, maxAccessTimes) = \left\{ \begin{array}{rl}
 true &\mbox{ if $usedTimes \le maxAccessTimes$} \\
  false &\mbox{ otherwise}
       \end{array} \right.
\f]		

- Parameters
  Name | Type |  Default | Description
  ---- | ---- |  ------- | ------------
usedTimes |int32|   0 |     Access Times has consumed
maxAccessTimes | int32 | 8 | Maximum access times allowed

- Actions
 
 
 Ad-hoc actions

 
| action     | Add Access Times | Set Access Times
----- | ---------------- | -----------------
ActId | ACT_ADD_ACCESSTIME (100) | ACT_SET_ACCESSTIME (101)
One Shot| Yes | Yes
Parameters | int32 addedAccessTime | int32 newAccessTime
Description| lic.maxAccessTimes += addedAccessTime | lic.maxAccessTimes = newAccessTime; lic.usedTimes = 0
Memo | Topping additional access times | Reset the access time, previously used times are cleared
--------------------
\subsection ssecExpireHardData Expire by Hard Date
- LicenseID: gs.lm.expire.hardDate.1
- Description: Protected entity can only be accessed within the preset date range
- Function:
\f[
f(t, t_0, t_1) = \left\{ \begin{array}{rl}
 true &\mbox{ if $t_0 \le t < t_1$} \\
 true &\mbox{ if $t \le t_1$ and $t_0$ is disabled} \\
 true &\mbox{ if $t_0 \le t$ and $t_1$ is disabled} \\
 false &\mbox{ otherwise}
       \end{array} \right.
\f]

- Parameters:
  Name  |  Type   | Default  | Description
  ----- | ------  | -------- | ------------
  timeBegin | utc_time |  now() | Starting point of time range
  timeEnd | utc_time | now() | Ending point of time range
  timeBeginEnabled | bool | false | Enable the \a timeBegin
  timeEndEnabled | bool | true | Enable the \a timeEnd

- Ad-hoc actions
| action     | Change Start Date  |   Change End Date    |
------------ | ------------------ | -------------------- |
ActId   | ACT_SET_STARTDATE (102) | ACT_SET_ENDDATE (103 |
Parameters| utc_time startDate  |  utc_time endDate   |
Description | lic.timeBegin = startDate | lic.timeEnd = endDate |

*Note*:

  If the action's date value(startDate/endDate) is not a valid date time, then the corresponding license parameter(timeBeginEnabled/timeEndEnabled)
is set to *true* to disable the old threshold value.

------------------------------------------------------------------------------------------
\subsection ssecExpirePeriod Expire by Period

- LicenseID: gs.lm.expire.period.1
- Description: Protected entity expires after it has been accessed more than a preset time period.
- Function:
\f[
f(t, t_0, T) = \left\{ \begin{array}{rl}
 true &\mbox{ if $t_0 \le t < t_0 + T$} \\
 false &\mbox{ otherwise}
       \end{array} \right.
\f]

Where \a t0 is the first time the protected entity was accessed, \a T is time period allowed.

###Period .vs. HardDate

\a t0 and \a t1 of *Expire by HardDate* license is statically *pre-defined* in project setting;
the \a t0 value of *Expire By Period* license, however, gets its value at runtime when the associated entity is accessed
for the very first time.

- Parameters:
  Name  |  Type  | Default  | Description
  ----- | ------ | -------- | ------------
  timeFirstAccess | utc_time | now() | Entity's first access time
  periodInSeconds | int32 | 60*60*8 (8 hours) | Time period allowed since first access
  
- Ad-hoc Actions
  Action  |  Sets Expire Period Length   |  Increments Expire Period Length
  ------- | -----------------------------|----------------------------------
  ActId  |ACT_SET_EXPIRE_PERIOD (105)    | ACT_ADD_EXPIRE_PERIOD (106)
  Parameters | int32 newPeriodInSeconds  | addedPeriodInSeconds
  Description| lic.periodInSeconds = newPeriodInSeconds | lic.periodInSeconds += addedPeriodOmSeconds
  

  ---------------------------------------------------------------------------------------------------------------

\subsection ssecExpireDuration Expire by Duration
- LicenseID: gs.lm.expire.duration.1
- Description: Protected entity expires after it has been accessed cultimatively more than a preset time duration.
- Function:
\f[
f(t_i, T) = \left\{ \begin{array}{rl}
 true &\mbox{ if $\displaystyle\sum_{i=1}^n t_i < T$ } \\
 false &\mbox{ otherwise}
       \end{array} \right.
\f]

Where \a ti is the \a i'th session time the protected entity was accessed, a \a T is time duration allowed.

- Parameters

  Name  |  Type  | Default  | Description
  ----- | ------ | -------- | ------------
 usedDurationInSeconds | int32 | 0 | Entity's cultimative access time
 maxDurationInSeconds | int32 | 60*60*8 (8 hours) | Maximum time duration allowed

- Ad-hoc Actions


  Action  |  Sets Expire Duration   |  Increments Expire Duration
  ------- | ----------------------- | ----------------------------------
 ActId   | ACT_SET_EXPIRE_DURATION (107) | ACT_ADD_EXPIRE_DURATION (108)
 Parameters | int32 duration        | int32 addedDuration
 Description | lic.maxDurationInSeconds = duration | lic.maxDurationInSeconds += duration

 -----------------------------------------------------------------------------------------------------------

\subsection ssecExpireSession Expire by Session Time
- LicenseID: gs.lm.expire.sessionTime.1
- Description: Protected entity expires after it has been accessed more than a preset session time.
- Function:
\f[
f(t,t_{start}, T) = \left\{ \begin{array}{rl}
 true &\mbox{ if $t_{start} \le t < t_{start} + T$} \\
 false &\mbox{ otherwise}
       \end{array} \right.
\f]

Where \a tstart is the start time the protected entity is accessed, \a T is maximum session time allowed.


- Parameters:

  Name  |  Type  | Default  | Description
  ----- | ------ | -------- | ------------
maxSessionTime | int32 | 180 (3 mins) | Maximum session time in seconds


- Ad-hoc Actions


  Action  |  Sets Session Time   
  ------- | ----------------------------------
  ActId  |  ACT_SET_SESSION_TIME (104)
  Parameters| int32 newSessionTime (180)
  Description| lic.maxSessionTime = newSessionTime

--------------------------------------------------------------------------
\subsection secAlwaysRun Always Run
-`LicenseID: gs.lm.alwaysRun.1
- Description: Protected entity can always be accessed. This license model can help to display some UI
						 feedbacks (splash screen/help screen,etc.) when the entity is being accessed.
- Function:
\f[ f(t) \equiv true; \f]

- Parameters: None


-------------------------------------------------------------------------
\subsection secAlwaysLock Always Lock
- LicenseID: gs.lm.alwaysLock.1
- Description: Protected entity cannot be accessed until it is unlocked/fully-purchased.
- Function:
\f[ f(t) \equiv false; \f]
- Parameters: None



\section tut_fp FingerPrint
GS5 rewrites the legacy finger print algorithms and now supports error-tolerant hardware detection. For example, if you set tolerance to 2, then two hardware parameters can be changed
without invalidating the license status.

  name | description | Windows (SDK v5.1) | Windows (SDK v5.2) | Unix (Mac, Linux) | Memo
 ------- | ----------- | ------- | ----------------- | --------- | ---------
RAM Size | Total RAM size installed in the system | Yes | No | Yes| |
RAM SN | Memory Chip Serial Number | No | Yes | No | |
Network Adaptor | Network MAC address | Yes | No | No | Not good for system with multiple network devices |
Hard Disk | Detect HardDisk parameters | Yes | Yes | No | Win/SDK5.1 detects the system HD, SDK 5.2 detects the first HD |
BIOS  | Detect BIOS date and version | Yes | Yes | No | |
CPU | Detect CPU type | Yes | Yes | Yes | |
Audio Device | Audio Device Information | Yes  | No | No | |
Video Card | Display Adaptor Information | Yes | No | No | |
Windows ProductId | Detects Windows ProductId | Yes | No | No | |
Host ID | Machine's unique host id | No | No | Yes | Warning: Might be changed by super user! |
Host Name | Network name of this machine | No | No | Yes | Warning: Might be changed by super user! |
Machine Type | The machine dependent architecture type | No | No | Yes | Intel, PowerPC, Sparc, etc.|
OS Version | Version level of the operating system | No | No | Yes | |
OS Name | Name of the operating system implementation | No | No | Yes | |
Machine Model | The unique machine hardware model | No | Yes | Yes | the most reliable fp feature.
Motherboard  | The motherboard serial number | No | Yes | No | |

SDK/Windows v5.2 has deprecated some fingerprints (Network Mac Address, Audio Info, Video Info, Windows Product Id) that cannot remain constant if multiple or mobile devices are used in the same machine. 



\section secExecutionMode Execution Mode
   \anchor ExecutionMode
  Execution Mode is an important project setting in GameShield, it specifies how the license model pops up its user interface to interact with the game player when wrapped game launches, expires or exits.
	First let us explain why we introduce execution mode in GS5. 
	
	By design, GS5 tries to be as non-intrusive as possible when integrating our DRM logic with the original game binaries, in most cases, we don't have access to the game's source code and cannot modify the game's original behaviour at source code level. However, our DRM logic (LMApp HTML UI, etc.) might have to interact with game player before game launches, while game running or after game existing. Depending on the game's nature or development tool-kits, there are chances that popping up a full-fledged Windows UI might cause unexpected conflicts with current game execution context. 
- Full Screen: If a game is a full screen video game, LMApp UI cannot be seen by end users; 
- COM Thread model mismatch: If a Windows game must run in a thread COM model (STA/MTA, etc.), the execution of LMApp might initialize the thread in another COM thread model, this is a common issue in GS4 because the GS4 license core (SSCProt.dll) is a COM dll, GS5 has tried to avoid this kind of conflict and gsCore is a pure DLL without using any COM technology, but it cannot guarantee that the COM thread model conflicting issue can be completely avoided because any other custom License model dll used to render LMApp UI might initialize a thread context before the game code starts.
- When game is terminating, the game process runtime could be in an unknown or unstable state (due to DLL unloading, etc.), the thread message queue might have been destroyed, it is very hard and prune to deadlock at this time to pop up a LMApp UI. 
- On Mac, creating a LMApp before the game executes might cause the app's default system menu displays undesired menu items, conflicts with game's main message loop. 
	
	
	The answer to this technical problem in GS5 is: executing LMApp logics in different process from game itself. 
	
	There are two categories of Execution Mode: *Single Process (P1S*)* and *Dual Process (P2)*.
	
	\subsection ssecP1 P1: One Process Single Pass
	\image html P1.png
	\image latex P1.eps "Execution Mode: P1" width=8cm
		
This is the most simple execution mode, LMApp and game runs in the same process, so all of the above mentioned conflicts might occur.
For a standard Windows GUI-based application or a simple game, P1 might work just fine. 
On Windows when game expires, Ironwrapper will send WM_CLOSE message to all game's topmost windows, so the game can save game data properly before LMApp pops up its UI.  
			
\subsection ssecP1S2 P1S2: One Process Two Passes
\image html P1S2.png
\image latex P1S2.eps "Execution Mode: P1S2" width=8cm
	
P1S2 is the behaviour of GameShield V4 wrapped games. In P1S2 mode, the LMApp's startup UI shares the same process with game itself, but the Exit UI pops up in a different *pass*, which means, when game expires or exists, the wrapped game restarts to render the Exit UI. It can make sure the Exit-UI can always be rendered properly.   
	
	  
\subsection ssecP1S3 P1S3: One Process Three Passes
\image html P1S3.png
\image latex P1S3.eps "Execution Mode: P1S3" width=8cm

In P1S3 mode, the LMApp logic and game logic are partitioned completely in three different process passes, when game starts in the first pass, the LMApp startup UI is rendered, if license gives positive result, the game restarts to enter the second pass running the game itself, after game terminates, the wrapped game restarts again to enter the third pass to render the LMApp's Exit UI.
	
P1S3 is the most safe and compatible execution mode because LMApp UI logic and game code never get a chance to co-exist in a same process, however, it does have performance degrade: end users might notice game launches multiple times. 	
	
\subsection ssecP2 P2: Dual Process
\image html P2.png
\image latex P2.eps "Execution Mode: P2" width=8cm

P2 mode is similar to P1S3 that LMApp UI logic and game code are isolated completely in different processes, however, unlike P1S3 restarting the game in multiple passes, P2 mode launches two game processes at the same time, one is game process and another one is a licensing process monitoring the game process, rendering Start/Exit UI as necessary.
	
	
\subsection ssecExpressMode Game Launch Express Mode
   To overcome LMApp UI Rendering and game logic conflicting issues, GS5 has introduced different execution modes, you can try each mode to find the best one for your game. For P1S2/P1S3 mode, as we have noticed in the previous section, the game has to restarted to render LMApp UI or run the game itself. 
   
   What if the game player has fully purchased the game? after the game has been activated, in most cases there is no need to go through the execution passes at all, so GS5 has introduced another control setting: *Game Launch Express Mode*. You can enable this feature in IDE, if this feature is turned on, when a fully activated game is double-clicked, the first pass double-check the license status first, if everything seems ok then the current pass becomes the game pass automatically, when game terminates, there is also no Exit-UI pops up.
   
   Game Launch Express Mode can be quite useful especially for Mac games, restarting game multiple times might cause the game's icon keeps bouncing on the system dock bar.
    





   \section Exe-Hopping Exe-Hopping
   \anchor Exe-Hopping



 *  \section api_sec SDK API Summary

   There are more than 100 APIs published from gsCore shared library, however most of them are related to Entity/License/Action reflection and could be rarely used if
   you are not developing high-level licensing toolkit.

   The APIs are grouped as following:

   - System initialize and cleanUp : ref to gsInit() and gsCleanUp()
   - Entity Accessing :  gsOpenEntity*, gsGetEntity*, etc.
     To evaluate if an entity is accessible before actually accessing it, you can call gsGetEntityAttributes() to see if the ENTITY_ATTRIBUTE_ACCESSIBLE is ON.
     <img src="gsEntityAccess.png"/>

     An entity is either <b>accessible</b> or <b>not accessible</b> at a time point depending on its current license status,
     also an entity can be <b>active</b> or <b>inactive</b>, when an entity is \a active, all of its associated resources (passwords, keys, files, chapters, etc.)
     can be accessed normally, otherwise those protected resources cannot be accessed legally. You can think of a door(the entity) with locks(attached licenses), usually the door
     is closed and all of the stuffs behind the door is access-denied, you want to inspect if you can enter the room by detecting the door's accessibility status (gsGetEntityAttributes()),
     if the result is positive, you can legally open the door via gsBeginAccessEntity(), thus the door is left open during the time the entity is being active /accessed by the visitors.
     If you have accessed what you want inside the room and do not want to use them any more, please close the door via gsEndAccessEntity(), as result the entity becomes \a inactive.

     You do not always have to call gsBeginAccessEntity() before accessing it. If you find the door is already widely open (in \a active state), you just enter the room freely. Maybe in
     the future relase I will add some synchronize/lock schema for multiple threads accessing, for now it just works fine for the current usage.

   - Entity Event Callback : Install /Uninstall the event callback for an entity.
      refer to gsAddEntityEventCallback(), gsRemoveEntityEventCallback();

   - License Accessing : gsOpenLicense*, gsGetLicenseParam*, etc.

   - Action setup: refer to gsGetActionParamCount(), gsGetActionParamByIndex(), gsGetActionParamByName().

   - Parameter/Variable accessing : gsGetVariableValueAs*(), gsSetVariableValueFrom*()
     <br>Please note that for license parameters (hParam is from gsGetLicenseParamByIndex()/gsGetLicenseParamByName()), you can only
     \a *read*, the set apis won't work for security reason. License status can only be modified legally from inside or by applying authorized license actions.


   - Request Code Composition: refer to gsCreateRequest(), gsGetRequestCode()

   - License Code Apply : Only one api: gsApplyLicenseCode()

   For each gsOpen* or gsCreate* api you get a \a handle, after using it the handle should be *released* by calling gsCloseHandle(). In the current implementation
   gsCloseEntity()/gsCloseLicense() are redirected to gsCloseHandle() via macro.


   \section secDLM Dynamic License Model
   \anchor DLM

   \subsection ssec_ParamAccessCtl Parameter Accessibility
   \anchor LMParamAccessCtl

 *  \section tut_sec Programming Tutorial
 *
 *   	\subsection ex1 Example 1: SDK Usage, License Status Checking
 *   In this sample, we demonstrate:
 *   - how to handle events by subclassing TGSApp;
 *   - how to initialize license by calling TGSCore::init();
 *   - how to use std::auto_ptr<> to keep the lifetime of SDK objects;
 *   - how to access an entity. First we check if the entity is accessible, if true, we begin access it, and then enter a loop to wait for expiration.
 *	   
 *
 *
 *  The sample uses a demo license file and you can <a href="gs.sample.18.lic">Download sample license file here</a>
 *  The license defines two entity (game.level1, game.level2), game.level1 is attached a license of type (LM_Expire_AccessTime)
 *  and game.level2 has a license of type (LM_Expire_SessionTime), the password is "123456".
 *
\code
#include "GS5_Ext.h" //include SDK header
#include <stdio.h> 
#include <assert.h>

using namespace gs; //the SDK namespace


const char* productId = "gs.sample.18";
const char* productLic = "gs.sample.18.lic";

class TMyApp: public TGSApp {
  DECLARE_APP(TMyApp);
private:
`  bool _expired;
protected:
    virtual void OnEntityAccessInvalid(TGSEntity* entity, bool inGame)
	{
		_expired = true; //License expired!
	}
public:
	TMyApp():TGSApp(), _expired(false){}

	int run()
	{
	  TGSCore * core = core(); // or TGSCore::getInstance();
	  core->init(productId, productLic, "123456", NULL);

	  std::auto_ptr<TGSEntity> entity(core->getEntityById("game.level2"));
		
	  if (entity->isAccessible()){
		puts("[game.level2] is ok!");
		if (entity->beginAccess()) {
			assert(entity->isAccessing());

			//dump license status
			std::auto_ptr<TGSLicense> lic(entity->getLicenseByIndex(0));
			printf("maxSessionTime = [%d]", lic->getParamInt("maxSessionTime"));

			puts("waiting for expiration... \n");
			while (!_expired) {
				sleep(1000); //1s
			}

			puts("Expired!... \n");
			entity->endAccess();
		}
	  }else{
		puts("[game.level2] cannot be accessed!");
		return -1;
	  }

	  return 0;
	}
};

IMPLEMENT_APP(TMyApp);

int main() 
{
  GS_INIT_APP;

  return GET_APP(TMyApp)->run();
}
\endcode
 *
 *   	\subsection ex2 Example 2: License Reflection
 *   \code
 *	//Dump all entities in memory
	{
		TGSCore * core = TGSCore::getInstance();
		int N = core->entityCount();
		for(int i = 0;i<N;i++){
			std::auto_ptr<TGSEntity> entity.reset(gsOpenEntityByIndex(i);
			cout << "Entity id [" << gsGetEntityId(hEntity) << "] "
				 << "name [" << gsGetEntityName(hEntity) << "] "
				 << "description [" << gsGetEntityDescription(hEntity) << "] "<<endl;

				 //Dump all licenses attached to this entity
				 int totalEntities = gsGetLicenseCount(hEntity);
				 for(int j = 0; j < totalEntities ; j++){
					 TLicenseHandle hLic = gsOpenLicenseByIndex(hEntity, j);
					 assert(hLic);

					 //Dump all license parameters
					 int totalParamers = gsGetLicenseParamCount(hLic);
					 for(int k =0; k< totalParamers; k++){
					   TParamHandle hParam = gsGetLicenseParamByIndex(hLic, k);

						//data type
						param_type_t pt = gsGetParamType(hParam);
						cout << "Param Type [" << gsParamTypeToString(pt) << "]" << endl;

						switch(pt){
							case PARAM_DWORD:
							{
								gs_dword v;
								BOOST_VERIFY(gsGetParamValue_dword(hParam,v));
								cout << "Param Value [" << v << "]" << endl;
								break;
							}
							case PARAM_FLOAT:
							{
								gs_float v;
								BOOST_VERIFY(gsGetParamValue_float((hParam),v));
								cout << "Param Value [" << v << "]" << endl;
								break;
							}
							...
						  }
						   gsCloseHandle(hParam);
						}
					 gsCloseLicense(hLic);
				 }
			gsCloseEntity(hEntity);
		}
	}
	\endcode
 *
 *
 *      \subsection ex3 Example 3: Request Code Composition
 *   \code
 *
 *
 void main(){
    gsInit(productId, productLic, "123456", intf);

	TEntityHandle hEntity = gsOpenEntityById("game.level2");
    // To reset the session time to 20 seconds
	TLicenseHandle hLic = gsOpenLicenseById(hEntity, "gs.lm.expire.sessionTime.1");
	TRequestHandle hReq = gsCreateRequest();

	TActionHandle hAct = gsAddRequestAction(hReq,ACT_SET_SESSIONTIME, hLic);

	TParamHandle hParam = gsGetActionParamByName(hAct, "newSessionTime");
	assert(hParam);
	gsSetParamValue_dword(hParam, 20); //20s
	gsCloseHandle(hParam);

	gsCloseAction(hAct);

	char reqCode[2048];
	size_t codeLen = 2048;
	gsGetRequestCode(hReq, reqCode, codeLen);
	gsCloseRequest(hReq);

	cout << "The ReqCode : " << reqCode << endl;

	gsCloseLicense(hLic);
	gsCloseEntity(hEntity);
	gsCleanUp();
}
 *
 *
 *   \endcode

 *   	\subsection ex4 Example 4: License Code Apply

\code
#include <iostream>
using namespace std;

#include "gsCore.h"

int main(int argc, char* argv[]) {
	using namespace gs;

	if(argc == 1){
		cout << "testLicActApply xxxxx"<<endl;
		return 0;
	}

	gsInit("SampleGame", "sampleGame.lic", "123456", NULL);
	gsApplyLicenseCode(argv[1]);
	gsCleanUp();

	return 0;
}
\endcode
 *
 */

#ifndef MAINPAGE_H_
#define MAINPAGE_H_


#endif /* MAINPAGE_H_ */
